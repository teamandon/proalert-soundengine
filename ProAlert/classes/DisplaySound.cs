﻿namespace SoundEngine.classes
{
    public class DisplaySound
    {
        public string Name { get; set; }
        public string RequestBy { get; set; }
    }
}
