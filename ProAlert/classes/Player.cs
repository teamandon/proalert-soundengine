﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Media;

namespace SoundEngine.classes
{
    public class Player
    {
        public event EventHandler LoadCompleted;
        private readonly SoundPlayer _sp;
        public string Name { get; set; }
        //public String Path { get; set; }
        public byte[] Sound { get; set; }
        public bool Loaded { get; set; }
        public int RepeatCnt { get; set; }
        public Player()
        {
            _sp = new SoundPlayer();
            _sp.LoadCompleted += player_LoadCompleted;
        }

        public void SetUp()
        {
            Loaded = false;
            _sp.Stream = new MemoryStream(Sound);
            //_sp.SoundLocation = Path;
            _sp.LoadAsync();
        }

        public void Play()
        {
            _sp.PlaySync();
        }

        private void player_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Loaded = true;

            var handler = LoadCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }

    public class PlayerList
    {
        private readonly List<Player> _list;
        public EventHandler AllLoadsComplete;
        public PlayerList()
        {
            _list = new List<Player>();
        }

        public void Add(Player p)
        {
            p.LoadCompleted += LoadCompleted;
            _list.Add(p);
        }

        public List<Player> GetList()
        {
            return _list;
        }

        public Player Find(string name)
        {
            return _list.Find(x => x.Name == name);
        }

        public string GetSoundList()
        {
            return string.Join("\n", _list.Select(x => x.Name).ToArray());
        }

        private void LoadCompleted(object sender, EventArgs e)
        {
            var complete = true;
            foreach (var item in _list.ToList())
            {
                if (!item.Loaded)
                    complete = false;
            }
            if (complete)
            {
                // raise all sounds loaded event
                AllLoadsComplete?.Invoke(this, e);
            }
        }

    }
}
