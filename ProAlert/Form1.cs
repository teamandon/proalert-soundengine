﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using Microsoft.AspNet.SignalR.Client;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using SoundEngine.classes;
using SoundEngine.forms;
using SoundEngine.Properties;
using Enumerations = ProAlert.Andon.Service.Common.Enumerations;

namespace SoundEngine
{
    public partial class Form1 : Form
    {
        #region <- Members ->
        //delegate void SetTextCallback(string text);
        delegate void SetDataSourceCallBack(object obj);
        private delegate void SetProgressCallBack(object obj);

        private bool _loaded = false;

        private System.Timers.Timer _timer;
        private int _counter;
        private string _connString;


        private Requests _requests;
        private Advisories _advisories;

        private PlayerList _playerList;
        private bool _tryingtoReconnect;
        private HubConnection _hubConn;

        private ConcurrentQueue<AudioFile> _soundQueue;
        private bool _queuePlaying;
        //private bool _refreshingDataGrid;
        private string _currentlyPlayingFile;

        private List<int> _bcWcs;
        private int _hold;
        #endregion
        public Form1()
        {
            InitializeComponent();
            _requests = new Requests();
            _advisories = new Advisories();

            LoadMessages();
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 20;
            txtFrequency.Text = Properties.Settings.Default.frequency.ToString();
            txtHold.Text = Properties.Settings.Default.hold.ToString();
            EnableTextboxes();
            InitHub();
            _soundQueue = new ConcurrentQueue<AudioFile>();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            _bcWcs = new List<int>();
            FillBcWcs();
            _hold = int.Parse(txtHold.Text);
        }

        #region <- Methods ->

        private void RegisterNotification()
        {
            _connString = System.Configuration.ConfigurationManager.ConnectionStrings["ProAlertContext"].ConnectionString;
            SqlDependency.Start(_connString);
            var commandText = new[]
            {
                @"select [CreatedDate] ,[ModifiedDate] ,[CreatedBy] ,[ModifiedBy] ,[MacId] ,[RowVersion] from dbo.automessage",
                @"select [CreatedDate] ,[ModifiedDate] ,[CreatedBy] ,[ModifiedBy] ,[MacId] ,[RowVersion] from dbo.audiofile"
            };
            using (var conn = new SqlConnection(_connString))
            {
                using (var command = new SqlCommand(string.Join("; ", commandText), conn))
                {
                    conn.Open();
                    var sqlDependency = new SqlDependency(command);
                    sqlDependency.OnChange += new OnChangeEventHandler(sqlDependency_OnChange);
                    using (var reader = command.ExecuteReader())
                    {
                    }
                }
            }
        }

        private void sqlDependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            _advisories.Load();
            RegisterNotification();
        }

        private void LoadMessages()
        {
            _advisories.Load();
        }
        private void CreatePlayers()
        {
            _playerList = new PlayerList();
            _playerList.AllLoadsComplete += AllLoadsComplete;
            try
            {
                using (var context = new ProAlertContext())
                {
                    using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                    {
                        foreach (var dr in repo.Get<AudioFile>().ToList())
                        {
                            //var stream = new MemoryStream(dr.Sound);
                            //var wf = new WaveFileReader(stream);
                            _playerList.Add(new Player
                            {
                                Name = dr.Name,
                                Sound = dr.Sound,
                                RepeatCnt = dr.RepeatCnt
                            });
                        }

                        foreach (var item in _playerList.GetList())
                        {
                            item.SetUp();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }
        private void DisableTextboxes()
        {
            txtFrequency.Enabled = false;
            txtHold.Enabled = false;
        }
        private void EnableTextboxes()
        {
            txtFrequency.Enabled = true;
            txtHold.Enabled = true;
        }
        private void SetProgress(object obj)
        {
            if (progressBar1.InvokeRequired)
            {
                var d = new SetProgressCallBack(SetProgress);
                Invoke(d, new object[] { obj });
            }
            else
            {
                progressBar1.Value = _counter;
            }
        }
        private void SetDataSource(object ds)
        {
            if (this.dataGridView1.InvokeRequired)
            {
                var d = new SetDataSourceCallBack(SetDataSource);
                Invoke(d, new object[] { ds });
            }
            else
            {
                dataGridView1.DataSource = ds;
            }
        }

        private void BuildRequestList()
        {
            if (_counter == 20)
                _counter = -1;
            _counter += 1;
            SetProgress(_counter);

            var tolerance = 5;
            if (_queuePlaying) return;
            var now = DateTime.Now;

            using (var context = new ProAlertContext())
            {
                using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                {
                    var query = (from x in repo.Get<CallLog>(x => x.ResponseDt == null, includeProperties: "Call, WorkCenter")
                                join a in repo.Get<AudioFile>() on new { AID = x.Call.AudioFileID } equals new { AID = a.Id }
                                join bc in _bcWcs on x.WorkCenterId equals bc
                                select new CallItem
                                {
                                    ID = x.Id,
                                    WorkCenter = x.WorkCenter.Name,
                                    Call = x.Call.Name,
                                    InitiateDT = x.InitiateDt,
                                    SoundName = a.Name,
                                    RepeatCnt = a.RepeatCnt
                                }).ToList();

                    foreach (var dr in query)
                    {
                        var secondDiff = (int)dr.InitiateDT.Subtract(now).TotalSeconds;
                        if (_hold >= 60)
                            if (Math.Abs(secondDiff) % _hold <= 59)
                            AddRequest(dr.SoundName, "WC: " + dr.WorkCenter + " Call: " + dr.Call);
                    }
                    // check AutoMessages for any sounds to play
                    foreach (var am in _advisories.GetList())
                    {
                        if (am.Reoccurring != true)
                        {
                            if (Math.Abs(now.Subtract(am.Start).TotalSeconds) < tolerance)
                            {
                                AddRequest(am.SoundName, am.Message);
                            }
                            continue;
                        }
                        if (am.Start > now) continue;
                        // With the change in the timer, I have changed '<=' to just '<'.  Tested on minutes.  nothing else.
                        switch (am.Interval)
                        {
                            case Enumerations.AMInterval.Minutes:
                                var secDiff = (int)am.Start.Subtract(now).TotalSeconds;
                                //if (Math.Abs(secDiff) % (am.IntervalQty * 60) < Properties.Settings.Default.frequency)
                                // Advisory messages do not get repeated by frequency setting.  Only Calls.
                                if (Math.Abs(secDiff) % (am.IntervalQty * 60) <= 5)
                                {
                                    AddRequest(am.SoundName, am.Message);
                                }
                                break;
                            case Enumerations.AMInterval.Hours:
                                var minDiff = (int)now.Subtract(am.Start).TotalSeconds;
                                if (minDiff % (am.IntervalQty * 3600) <= 5)
                                {
                                    AddRequest(am.SoundName, am.Message);
                                }
                                break;
                            case Enumerations.AMInterval.Days:
                                var hrDiff = (int)now.Subtract(am.Start).TotalSeconds;

                                if (hrDiff % (am.IntervalQty * 86400) <= 5)
                                {
                                    AddRequest(am.SoundName, am.Message);
                                }
                                break;
                            case Enumerations.AMInterval.Weeks:
                                var weekDiff = (int)now.Subtract(am.Start).TotalSeconds;
                                if (weekDiff % (am.IntervalQty * 604800) <= 5)
                                {
                                    AddRequest(am.SoundName, am.Message);
                                }
                                break;
                        }
                    }
                    foreach (var sf in _requests.Get())
                    {
                        var audioFile = repo.GetOne<AudioFile>(x => x.Name == sf.SoundName);
                        if (!_soundQueue.Contains(audioFile))
                        {
                            _soundQueue.Enqueue(audioFile);
                        }
                    }
                    if (!_queuePlaying && _soundQueue.Count > 0)
                    {
                        PlayFileAsync();
                    }

                }
            }
        }

        private void RefreshDataGrid(object ds)
        {
            SetDataSource(null);
            SetDataSource(ds);
            //_refreshingDataGrid = false;
        }

        private void AddRequest(string request, string by)
        {
            _requests.Add(new Request { SoundName = request, RequestBy = new StringBuilder(by)});
        }

        //private void AddAutoMessageSound(DateTime now, seAutoMessage am)
        //{
        //    AddActive(now, am);

        //    _sounds.Add(new SoundFile
        //    {
        //        InitiatedDT = now,
        //        RemainingPlays = am.RepeatCnt,
        //        RepeatCnt = am.RepeatCnt,
        //        SoundName = am.SoundName
        //    });
        //}
        #endregion

        #region <- Functions ->
        public bool IsNumber(string value)
        {
            if (value.Length == 0) return false;
            return !value.ToCharArray().Where(x => !Char.IsDigit(x)).Any();
        }


        #endregion

        #region <- Events ->
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!_loaded)
            {
                MessageBox.Show(Resources.Form1_btnStart_Click_sounds_have_not_loaded_yet);
                return;
            }

            _timer = new System.Timers.Timer {AutoReset = false};
            _timer.Elapsed += new ElapsedEventHandler(TimeElapsed);
            _timer.Start();
            RegisterNotification();
            DisableTextboxes();
        }
        private double GetInterval()
        {
            var now = DateTime.Now;
            return (60 - now.Second) * 1000 - now.Millisecond;
        }
        private void TimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                BuildRequestList();
            }
            catch (Exception ex)
            {
                using (var context = new ProAlertContext())
                {
                    using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                    {
                        repo.InsertError(new AppError
                        {
                            ErrorDT = DateTime.UtcNow,
                            ErrorMsg = "build request: " + ex.InnerException.ToString(),
                            Source = "Sound Engine"
                        });
                        repo.Save();
                    }
                }
            }
            _timer.Interval = GetInterval();
            _timer.Start();
        }
        private void txtFrequency_TextChanged(object sender, EventArgs e)
        {
            if (!IsNumber(txtFrequency.Text)) return;
            Properties.Settings.Default.frequency = int.Parse(txtFrequency.Text);
            Properties.Settings.Default.Save();
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            _timer?.Dispose();
            progressBar1.Value = 0;
            EnableTextboxes();
            //_sounds.Clear();
            //_callList.ClearAll();
        }
        private void AllLoadsComplete(object sender, EventArgs e)
        {
            _loaded = true;
            btnStart_Click(null, null);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            TitleReset();
            CreatePlayers();
        }
        private void txtHold_TextChanged(object sender, EventArgs e)
        {
            if (!IsNumber(txtHold.Text)) return;
            Properties.Settings.Default.hold = int.Parse(txtHold.Text);
            Properties.Settings.Default.Save();
            _hold = Settings.Default.hold;
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new AboutBox1();
            frm.ShowDialog(this);
        }
        private void loadMessagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadMessages();
        }
        private void showSoundListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(_playerList.GetSoundList(), "All Loaded Sound Files");
        }
        #endregion

        #region <- SignalR ->
        private void InitHub()
        {
#if DEBUG
            //_hubConn = new HubConnection("http://localhost:59611/");
            _hubConn = new HubConnection("http://proalert.hopto.org/");

#else
           //_hubConn = new HubConnection("http://proalert.hopto.org/");

            //_hubConn = new HubConnection("http://proalert.vistechpa/");
            //_hubConn = new HubConnection("http://pawc.vistechpa/");
            //_hubConn = new HubConnection("http://192.168.1.20/");
            //_hubConn = new HubConnection("http://192.168.1.4/");
            //_hubConn = new HubConnection("http://Bstserver/");
            //_hubConn = new HubConnection("http://192.168.1.115/");  // RHA
            //_hubConn = new HubConnection("http://proalert.abitape.net/"); // ABI Tape
            //_hubConn = new HubConnection("http://ipcproalert/"); // IPC
            _hubConn = new HubConnection("http://proalert.amtexna.local/");
#endif

            var adProxy = _hubConn.CreateHubProxy("andonDisplay");
            adProxy.On<int>("callInitiate", call => InitiateCall(call));
            adProxy.On<int>("clearCall", wccid => ClearCall(wccid));
            //adProxy.On<int>("warningAdvisory", wcId => SoundWarning(wcId));
            //adProxy.On<Call>("callRespond", call => RespondCall(call));
            _hubConn.StateChanged += HubConnOnStateChanged;
            _hubConn.ConnectionSlow += HubConnOnConnectionSlow;
            _hubConn.Closed += HubConnOnClosed;
            _hubConn.Reconnecting += HubConnOnReconnecting;
            _hubConn.Reconnected += HubConnOnReconnected;
            _hubConn.Start().Wait();
        }
        private void HubConnOnStateChanged(StateChange stateChange)
        {
            var msg = "Old State: " + stateChange.OldState + " New State: " + stateChange.NewState;
            //MessageBox.Show(msg);
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    repo.InsertEvent(new AppEvent
                    {
                        EventDT = DateTime.UtcNow,
                        EventMsg = "Hub Event: " + msg, 
                        Source = "Sound Engine"
                    });
                    repo.Save();
                }
            }
        }
        public void InitiateCall(int wccid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    using (var roContext = new ProAlertContext())
                    {
                        using (var roRepo =
                               new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(roContext))
                        {
                            var call = roRepo.GetOne<WorkCenterCall>(x => x.Id == wccid, "Call, WorkCenter");
                            if (!_bcWcs.Contains(call.WorkCenterID)) return;  // don't play sounds for WCs that aren't apart of this Andon Center.
                            var audio = roRepo.GetById<AudioFile>(call.Call.AudioFileID);
                            if (_currentlyPlayingFile == audio.Name) return;
                            try
                            {
                                if (!_requests.Exists(audio.Name))
                                {
                                    AddRequest(audio.Name, call.WorkCenter.Name + " Call: " + call.Call.Name);
                                    _soundQueue.Enqueue(audio);
                                    if (!_queuePlaying)
                                        PlayFileAsync();
                                }
                            }
                            catch (Exception e)
                            {
                                repo.InsertEvent(new AppEvent
                                {
                                    EventDT = DateTime.UtcNow,
                                    EventMsg = "Hub Error: " + e.Message,
                                    Source = "Sound Engine"
                                });
                                repo.Save();
                            }
                        }
                    }

                }
            }
        }

        public void SoundWarning(int wcId)
        {
            using (var roContext = new ProAlertContext())
            {
                using (var roRepo =
                       new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(roContext))
                {
                    var wc = roRepo.GetById<WorkCenter>(wcId);
                    if (!_bcWcs.Contains(wcId)) return;  // don't play sounds for WCs that aren't apart of this Andon Center.
                    // get the ClearCall sound file.
                    var audio = roRepo.GetFirst<AudioFile>(x => x.Name.Equals("fail"));
                    if (_currentlyPlayingFile == "fail") return;
                    try
                    {
                        if (!_requests.Exists(audio.Name))
                        {
                            AddRequest(audio.Name, wc.Name + " Call: Fail");
                            _soundQueue.Enqueue(audio);
                            if (!_queuePlaying)
                                PlayFileAsync();
                        }
                    }
                    catch (Exception e)
                    {
                        using (var context = new ProAlertContext())
                        {
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                repo.InsertEvent(new AppEvent
                                {
                                    EventDT = DateTime.UtcNow,
                                    EventMsg = "Hub Error: " + e.Message,
                                    Source = "Sound Engine"
                                });
                                repo.Save();
                            }
                        }
                    }

                }
            }
        }
        public void ClearCall(int wccid)
        {
            using (var roContext = new ProAlertContext())
            {
                using (var roRepo =
                       new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(roContext))
                {
                    var call = roRepo.GetOne<WorkCenterCall>(x => x.Id == wccid, "Call, WorkCenter");
                    if (!_bcWcs.Contains(call.WorkCenterID) || !call.Call.DT) return;  // don't play sounds for WCs that aren't apart of this Andon Center.
                    if (call.Call.Type != Enumerations.CallType.Maintenance) return;
                    // get the ClearCall sound file.
                    var audio = roRepo.GetFirst<AudioFile>(x => x.Name.Equals("reveille"));
                    if (_currentlyPlayingFile == "reveille") return;
                    try
                    {
                        if (!_requests.Exists(audio.Name))
                        {
                            AddRequest(audio.Name, call.WorkCenter.Name + " Call: Reveille");
                            _soundQueue.Enqueue(audio);
                            if (!_queuePlaying)
                                PlayFileAsync();
                        }
                    }
                    catch (Exception e)
                    {
                        using (var context = new ProAlertContext())
                        {
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                repo.InsertEvent(new AppEvent
                                {
                                    EventDT = DateTime.UtcNow,
                                    EventMsg = "Hub Error: " + e.Message,
                                    Source = "Sound Engine"
                                });
                                repo.Save();
                            }
                        }
                    }

                }
            }
        }
        /// <summary>
        /// The hope is that we can continuely stuff/queue sound files and it will play them in order.  
        /// We will do conditional Enqueueing.  Check to see if the sound name already exists in the list. Theoretically, it could endup playing the same sound back to back.  If so, don't add again.
        /// </summary>
        private async Task PlayFileAsync()
        {
            if (!_soundQueue.TryDequeue(out AudioFile nextSound))
            {
                _queuePlaying = false;
                return;
            }

            _currentlyPlayingFile = nextSound.Name;

            _queuePlaying = true;
            var displayList = _requests.Get().Select(s => new DisplaySound
            {
                Name = s.SoundName,
                RequestBy = s.RequestBy.ToString()
            }).ToList();

            RefreshDataGrid(displayList);

            var p = _playerList.Find(nextSound.Name);

            for (var i = 0; i < nextSound.RepeatCnt; i++)
            {
                // p.Play() is a synchronous method. We'll call it on a separate thread 
                // with Task.Run to avoid blocking. 
                // Be aware that this can have unintended side effects in GUI applications.
                await Task.Run(() => p.Play());
            }

            // If there are more sounds in the queue, play them
            if (!_soundQueue.IsEmpty)
            {
                await PlayFileAsync();
            }

            _currentlyPlayingFile = null;
            _queuePlaying = false;
            _requests.Clear();
            displayList.Clear();
            RefreshDataGrid(displayList);
        }

        private void HubConnOnClosed()
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    repo.InsertEvent(new AppEvent
                    {
                        EventDT = DateTime.UtcNow,
                        EventMsg = "Hub Event: Connection Closed",
                        Source = "Sound Engine"
                    });
                    repo.Save();
                }
            }

            if (!_tryingtoReconnect)
            {
                _tryingtoReconnect = true;
                InitHub();
            }
        }
        private void HubConnOnConnectionSlow()
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    repo.InsertEvent(new AppEvent
                    {
                        EventDT = DateTime.UtcNow,
                        EventMsg = "Hub connection is slow",
                        Source = "Sound Engine"
                    });
                    repo.Save();
                }
            }
        }
        private void HubConnOnReconnected()
        {
            _tryingtoReconnect = false;
        }
        private void HubConnOnReconnecting()
        {
            _tryingtoReconnect = true;
        }
#endregion

        private void selectCenterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _bcWcs.Clear();
            var frm = new frmSelectCenter();
            using (var context = new ProAlertContext())
            {
                using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                {
                    var bcs = repo.Get<BusinessResource>(x => x.AndonHead == true);
                    frm.BusinessResources = bcs.ToList();
                    frm.ShowDialog(this);
                    if (frm.bcId <= 0) return;
                    var list = repo.GetBobrByParent(frm.bcId);
                    Settings.Default.bcName = repo.GetById<BusinessResource>(frm.bcId).Name;
                    Settings.Default.bcID = frm.bcId;
                    Settings.Default.Save();
                    foreach (var item in list.List)
                    {
                        // we will only play associated to calls for the wcs in the list.
                        if (item.WC && !_bcWcs.Contains(item.WorkCenterID))
                            _bcWcs.Add(item.WorkCenterID);
                    }
                    lblBC.Text = Settings.Default.bcName;
                }
            }
        }

        private void FillBcWcs()
        {
            if (Settings.Default.bcID < 0) return;
            using (var context = new ProAlertContext())
            {
                using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                {
                    var list = repo.GetBobrByParent(Settings.Default.bcID);

                    foreach (var item in list.List)
                    {
                        // we will only play associated to calls for the wcs in the list.
                        if (item.WC && !_bcWcs.Contains(item.WorkCenterID))
                            _bcWcs.Add(item.WorkCenterID);
                    }
                    lblBC.Text = Settings.Default.bcName;
                } 
            }
        }

        private void TitleReset()
        {

            this.Text = "ProAlert Sound Control v. " + Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}
