﻿namespace SoundEngine.classes
{
    public class Enumerations
    {
        public enum CallType
        {
            Maintenance,
            TLGL,
            Materials
        }

        public enum DisplayLine
        {
            Maintenance = 1,
            TLGL = 2,
            Materials = 3,
            Advisory = 4
        }

        public enum AMInterval
        {
            Minutes = 0,
            Hours = 1,
            Days = 2,
            Weeks = 3
        }

        public enum DisplayColors
        {
            Green,
            Yellow,
            Red
        }

    }
}
