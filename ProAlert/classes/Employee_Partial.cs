﻿using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Models;

namespace SoundEngine.classes
{
    public class Employee_Partial : Employee 
    {
        public new TimeZoneInfo GetTimeZoneInstance()
        {
            if (_timeZoneInstance == null)
            {
                try
                {
                    _timeZoneInstance = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
                }
                catch
                {
                    TimeZone = "Eastern Standard Time";
                    _timeZoneInstance = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
                }
            }
            return _timeZoneInstance;
        }

        public void SetTimeZoneIntance(TimeZoneInfo value)
        {
            _timeZoneInstance = value;

        }

        // timezone conversions
        private TimeZoneInfo _timeZoneInstance;
        /// <summary>
        /// The users TimeZone using .NET TimeZoneNames
        /// </summary>
        [Display(Name = "Time Zone"), StringLength(50)]
        public string TimeZone
        {
            get { return _timeZone; }
            set
            {
                SetTimeZoneIntance(null);
                _timeZone = value;
            }
        }
        private string _timeZone;
    }
}
