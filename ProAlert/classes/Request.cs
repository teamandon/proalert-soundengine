﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SoundEngine.classes
{
    public class Request
    {
        public string SoundName { get; set; }
        public StringBuilder  RequestBy { get; set; }

    }

    public class Requests
    {
        private readonly List<Request> _list;

        public Requests()
        {
            _list = new List<Request>();
        }

        public bool Exists(string soundName)
        {
            return _list.Any(x => x.SoundName == soundName);
        }

        public void Add(Request r)
        {
            var o = _list.FirstOrDefault(x => x.SoundName == r.SoundName);
            if (o == null)
                _list.Add(r);
            else
                o.RequestBy.Append(r.RequestBy);
        }

        public List<Request> Get() => _list;

        public void Clear()
        {
            _list.Clear();
        }
    }
}
