﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace SoundEngine.classes
{
    public class ActiveAM
    {
        public int ID { get; set; }
        public DateTime InitiateDT { get; set; }
        public int Duration { get; set; }
    }

    public class ActiveAMs
    {
        private readonly List<ActiveAM> _list;

        public ActiveAMs()
        {
            _list = new List<ActiveAM>();
        }

        public void Add(ActiveAM am)
        {
            if (_list.FindAll(x => x.ID == am.ID).Count == 0)
            {
                _list.Add(am);
            }
        }

        public bool Exists(seAutoMessage am)
        {
            return _list.Any(x => x.ID == am.Id);
        }

        /// <summary>
        /// Tests to see if the duration has passed before removing item from list
        /// </summary>
        public void Clear()
        {
            // test to see if the duration has passed
            var now = DateTime.Now;
            foreach (var am in _list.ToList())
            {
                if (am.InitiateDT.AddMinutes(am.Duration) < now)
                    _list.Remove(am);

            }
        }
    }
    public class CallItem
    {
        public int ID { get; set; }
        public string WorkCenter { get; set; }
        public string Call { get; set; }
        public DateTime InitiateDT { get; set; }
        public string SoundName { get; set; }
        public int RepeatCnt { get; set; }
    }

    public class CallList
    {
        private readonly List<CallItem> _list;

        public CallList()
        {
            _list = new List<CallItem>();
        }

        public bool Add(CallItem pi)
        {
            if (_list.All(x => x.ID != pi.ID))
            {
                _list.Add(pi);
                return true;
            }
            return false;
        }

        public bool Remove(CallItem pi)
        {
            var item = _list.FirstOrDefault(x => x.ID == pi.ID);
            if (item != null)
            {
                try
                {
                    _list.Remove(item);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public List<CallItem> GetList()
        {
            return _list;
        }

        public void Clear(Sounds wl)
        {
            using (var context = new ProAlertContext())
            {
                var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context);
                foreach (var ci in _list.ToList())
                {
                    var responded = repo.Get<CallLog>().Any(x => x.Id == ci.ID && x.ResponseDt != null);
                    if (responded)
                        _list.Remove(ci);
                }
            }
        }

        public void ClearAll()
        {
            _list.Clear();
        }
    }

    public class SoundFile
    {
        public DateTime InitiatedDT { get; set; }
        public string SoundName { get; set; }
        public int RemainingPlays { get; set; }
        public int RepeatCnt { get; set; }
        public bool Delete { get; set; }
    }

    public class Sounds
    {
        private readonly List<SoundFile> _list;
        private readonly int _holdBeforeRepeat;

        public bool Playing { get; set; }

        public Sounds()
        {
            _list = new List<SoundFile>();
            LastCount = 0;
            _holdBeforeRepeat = Properties.Settings.Default.hold;
        }

        public void Add(SoundFile wf)
        {
            var owf = _list.FirstOrDefault(x => x.SoundName == wf.SoundName);
            if (owf == null)
            {
                wf.InitiatedDT = DateTime.Now;
                _list.Add(wf);
            }
            else
            {
                owf.RemainingPlays = wf.RemainingPlays;
                owf.InitiatedDT = DateTime.Now;
            }
        }

        public List<SoundFile> GetList()
        {
            return _list;
        }

        public List<SoundFile> GetPlayList(CallList cl)
        {
            // remove call from play list if 5 minutes has passed.
            // this will allow the call to repeat if it has not been responded to.
            foreach (var dr in _list.Where(x => x.RemainingPlays == 0).ToList())
            {
                dr.Delete = true;
                //MessageBox.Show(DateTime.UtcNow.ToString() + " blah " + DateTime.UtcNow.Subtract(dr.InitiatedDT).TotalSeconds.ToString() + " : " +
                //                _HoldBeforeRepeat.ToString());
                if (cl.GetList().Any(x => x.SoundName == dr.SoundName))
                {
                    dr.Delete = false;
                    if (DateTime.Now.Subtract(dr.InitiatedDT).TotalSeconds > _holdBeforeRepeat)
                    {
                        dr.RemainingPlays = dr.RepeatCnt;
                        dr.InitiatedDT = DateTime.Now;
                    }
                }
            }

            foreach (var sf in _list.Where(x => x.Delete))
            {
                _list.Remove(sf);
            }

            var newlist = new List<SoundFile>();

            foreach (var dr in _list.Where(x => x.RemainingPlays > 0).OrderBy(x => x.SoundName).ThenByDescending(x => x.RemainingPlays))
            {
                if(!newlist.Exists(x => x.SoundName == dr.SoundName))
                    newlist.Add(dr);
                //dr.RemainingPlays -= 1;
            }
            return newlist;
        }

        public void UpdateRemaining(SoundFile wf)
        {
            var wfList = _list.FindAll(x => x.SoundName == wf.SoundName);
            foreach (var dr in wfList)
            {
                dr.RemainingPlays -= 1;
            }
        }

        public void ZeroOutRemaining(SoundFile wf)
        {
            var wfList = _list.FindAll(x => x.SoundName == wf.SoundName);
            foreach (var dr in wfList)
            {
                dr.RemainingPlays = 0;
            }
        }
        public void ZeroOutRemaining(string name)
        {
            var wfList = _list.FindAll(x => x.SoundName == name);
            foreach (var dr in wfList)
            {
                dr.RemainingPlays = 0;
            }
        }

        public bool Exists(string name)
        {
            return _list.Any(x => x.SoundName == name);
        }

        public SoundFile Find(string name)
        {
            return _list.Find(x => x.SoundName == name);
        }
        public bool Remove(SoundFile wf)
        {
            var item = _list.FirstOrDefault(x => x.SoundName == wf.SoundName);
            if (item != null)
            {
                try
                {
                    _list.Remove(wf);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                    throw;
                }
            }
            return false;
        }

        public int LastCount { get; set; }

        public void Clear()
        {
            _list.Clear();
        }
    }
}
