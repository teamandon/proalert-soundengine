﻿using System;
using System.Linq;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace SoundEngine.tools
{
    public class TimezoneTools
    {
        public static DateTime UtCtoClient(DateTime utc)
        {
            using (var context = new ProAlertContext())
            {
                var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context);
                var emp = repo.Get<Employee>().FirstOrDefault(x => x.LogIn == "tzi");
                if (emp != null)
                {
                    var tzi = TimeZoneInfo.FindSystemTimeZoneById(emp.TimeZone);
                    if (tzi == null || utc == DateTime.MinValue) return DateTime.MinValue;

                    var userOffset = tzi.GetUtcOffset(utc).TotalHours;
                    var client = utc.AddHours(userOffset);
                    return client;
                }
            }
            return utc;
        }
    }
}
