﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using SoundEngine.tools;

namespace SoundEngine.classes
{
    public class seAutoMessage : AutoMessage
    {
        public int RepeatCnt { get; set; }
        public string SoundName { get; set; }
    }

    public class Advisories 
    {
        private readonly List<seAutoMessage> _list;

        public Advisories()
        {
            _list = new List<seAutoMessage>();
        }

        private void Add(seAutoMessage am)
        {
            _list.Add(am);
        }

        /// <summary>
        /// Adjusts to current Time Zone offset
        /// </summary>
        public void Load()
        {
            try
            {
                Clear();
                using (var context = new ProAlertContext())
                {
                    using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                    {
                        var AmQuery = from a in repo.Get<AutoMessage>(includeProperties: "AudioFile")
                            select new seAutoMessage
                            {
                                Id = a.Id,
                                Duration = a.Duration,
                                Start = a.Start,
                                Reoccurring = a.Reoccurring,
                                Interval = a.Interval ?? 0,
                                IntervalQty = a.IntervalQty ?? 0,
                                RepeatCnt = a.AudioFile.RepeatCnt,
                                SoundName = a.AudioFile.Name,
                                Message = a.Message
                            };
                        foreach (var am in AmQuery)
                        {
                            // adjust time to current time zone offset
                            am.Start = TimezoneTools.UtCtoClient(am.Start);
                            if (am.Start >= DateTime.Now || am.Reoccurring)
                                Add(am);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.InnerException == null ? "" : ex.InnerException.Message);
            }

        }

        public void Clear()
        {
            _list.Clear();
        }

        public List<seAutoMessage> GetList()
        {
            return _list;
        }
    }
}
