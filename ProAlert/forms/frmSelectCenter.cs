﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ProAlert.Andon.Service.Models;

namespace SoundEngine.forms
{
    public partial class frmSelectCenter : Form
    {
        #region < Members >
        public List<BusinessResource> BusinessResources { get; set; }
        public int bcId { get; set; }
        #endregion
        public frmSelectCenter()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bcId = -1;
            Close();
        }

        private void frmSelectCenter_Load(object sender, EventArgs e)
        {
            cbxBC.DataSource = BusinessResources;
            cbxBC.DisplayMember = "Name";
            cbxBC.ValueMember = "Id";
        }

        private void cbxBC_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var obj = (ComboBox) sender;
            bcId = (int) obj.SelectedValue;
        }
    }
}
